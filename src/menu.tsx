import React from 'react'
import { Grid } from '@material-ui/core'
import { IMenu, IMenuState } from './models/interfaces'
import * as S from './styles/MenuStyles'

export class Menu extends React.Component<IMenu, IMenuState> {
    public constructor(props: IMenu) {
        super(props)
        this.state = {
            opened: false,
        }
    }
    private toggle = () => {
        this.setState({ opened: !this.state.opened })
    }
    public render(): React.ReactElement {
        const { items } = this.props
        return (
            <div>
                <S.Hamburger onClick={this.toggle} opened={this.state.opened}>
                    <S.ImageWithText src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAA7EAAAOxAGVKw4bAAABSklEQVRoge2ZzWrDMBCEZ0zoocfSQ+hT9O176APFh9BTyCGUTg+tElmRf2Rra0z3g+BBsqJdyYJZBDjOIijpTdITyU6HJIS2VBdNkPxvZT4o6QBgP3XEUGI5bUzbLFnRKdqapm8ySdnfHOJxtXWTdsSQvK5mrEsp3bUS3aQdYwPnYLkDlPQK4GFRhOtxWTsAx3Ecx/nfUNIjfj2RBaE2uHqXuvrrrqCJX6iZhEHwANDu0lWaEnhJVVZawZXqjp2Og++zsel7Y9qyFpB0K2hyQea2byi5oaQtIHk7vLkgQ9vSg5gmVEsDwA7AmeQpTBon1NdWqg3PwRmO4ziO4zjzoaQX/HiiO4b8/VQzZ3zZ8UlJB0n7MOlYAnO0Ie3m7XTnjiwNIDyXWmsrOw1Ed2S5Yr7WSplfMYWGLX5ClPQu6fkPDpwFx7UDcDbPN2GhEG1fpCH3AAAAAElFTkSuQmCC" />
                    <S.Span>MENU</S.Span>
                </S.Hamburger>
                <S.Overlay onClick={this.toggle} opened={this.state.opened} />
                <S.Container opened={this.state.opened}>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <S.Input placeholder='E.g "Star Wars"' />
                        </Grid>
                        <Grid item xs={3} />
                        <Grid item xs={3}>
                            <S.Image src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAFgklEQVRoge2Z34td1RXHP+swDCFIHmIoIYhIkUEGmYYgbQlFxFIFkTxIaemDUmnpY1tsQWmfSimllP4BfeiLUoq/wKilqFiKmDS1SmM61ZjYGKJoMg3+aJtpnMysTx/OPs7x5t4z596ZOD7kCxfW2fvstdc6e+3168JlXMa6EJk5GxFzwAngNHAWWIyIzZWsJ6Yi4hvAj9SFiJgHjgKvqUcKfTYiclOl7IL6jC1kpuo59Y3M/HVm7txsGTuhfl99Qn05M8+oH2am5feeevNmy7gm1Gn1WvUO9T71QDkJ1Z9vtnxjQa0yc1/Lql5Tq82WaxSGuhp1CjiuXlOGHqflmVRaXirLrwvZXjOMVhPI1nh2jC8Xvm+O9JWZ+auIuGeIwJ8mPDbSNCLiD8C/C72uXdRLRf9lqmPfV4Dn1NtLHPgj8Cyw1Bwv0BzlUBMq72X7AwyhK6AaMU5EVGoVEVMRcSVwF7ALWAQOjpRenVLvy8z/lcv8gLq9Q+FLDnW3+mrxkH9XZ7tMaBk4BJwux7UXuPoTknUo1BlgRzmh14F3u0wI9RBwOCKuAT4L3AIcbs3fCGztsXGnB+qigbPqfEQsq58rVpDAPHXe1o3M/GYrJvy1HRPKca6oK+rKMHqt+R70/szcpe5QHyxyvJeZd0G5KF2IiCfVxfK4G7huYL5i9SJeRK8134NuTmM7cG3Z9t2IeB2g04TK4rPqI9S3fwq4W723eKZDwKmu9a1A1fAblz4CnAdm1OvK2NvqK70UKNivfi0itqg3RsRO4G3gWz3Xr0sJ6kg+FxFby0c5GhHv992bzJxRXyr2d0a9o/fiDYL62+YiZua3m/G+J3BKfbpUbp8BvlKe/1uYV6za50bgPHA6IpZa/G8qJ5IR8aexFKiq6rx6gNUouBe4irpiA5gGHqBcvjKWQNWK2uO40GPqj4GThdfV6s7yzkJrvPcJALxAfWF3UXuiuZYCqHsi4iJ+a6QRo+gqIra02Hy+8UjAi61stL8CEbGgPgp8UZ2OiLsz86Gqqpr5Ew6pGyZMBN8CGvNB/XKLz7PtF8c5AYDH1J9FxLT6pYjYRe2NltQ7NzDlXix8AbaxGnsWrZsNk0Gt1AOtyPyddQraZ8/d6qvF+/xDnW3Pj1UqluD1G1bT6K9n5rYNknUUrqf2fJS2z0J7clwTAngROKHORMRsRNybmW+WDbIdiAqG1QoJnQlcs6YCbgW2qcsRcQz4WACbRIFTwMGImKH+Mvc0XsGBiqkIlAPCdaLx9fBRnjVN3YB7Xz3O6ulPpkBh9AywD9iubhnmEtvKjBJ+2LoORReA+cbrTaxAwUHqYLK95CyHgWNNF6EpI8uXb8yhb3ty8L0sPI9TFzEbosAp6kx0T3k+EhE/aH91SitkjGx0lIJNJF+KiPMTynsxinu7UOrTf6lXbBjzTwLWHbyXi39W3bcZckxqQo3LfBiYK6bwvczcEREngOfb+UoflDRklrrqWwJeiIiTk8rXd9Mb1HdaeXpDvqTeYt2i7MNnLjOfUD9sRfkLmfnTzNxxKRW4PTPPtARv2vJaNwDmrDvfXb+t6u8zc2WQj/qB+kN1yygZ1pV9Zeb+iBhq++oyML9W6WfdddurTo2IyoeAO5sifhATK5CZFfABcEWP3s7EsO6I3FpV1fPD5ifu+7faJn2K8mF0jhj/GB0RH7VXhmFiL1RM5Ciwp6O6uh/4XaGT1ajavFJFxB7gFx2n95bavwMxphJfLUFs0AutqH9Tr+/Bo1KfUi84gMw8p/5EXbN9OakC2zLzu+o/B7zQnzPztnHcqPqo9b+jDc6pv1Sv6lq77luWmVsjYka9KSKuVN8AnouIk32DWXEIOyPiBuALwH+AI9Rp+6Uxn8u4jBr/BxxDFYrgkAzSAAAAAElFTkSuQmCC" />
                            <S.ImageContainer onClick={this.toggle}>
                                <S.ImageWithText src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAA7EAAAOxAGVKw4bAAADnElEQVRogcWasWobQRCGD6EqpAipzSGCESpSCJMiGL9ncJE6BGNCCCYE48oIE1IYF0bPIoz+L8WO5L3T3d7u7Z39g2EK3ezM7neztzsuihZJmkr6JOkrcAy0/XRwAQWwAL5JOgWmfZx8Bm6BLfBH0hKYjBBvRZImNta1pC2wAk6TnAAz4N6CR9KTOZyPFLc/9hy4Bp5w2kq6B2YxD08lndoDTXq0pR18JYBC0lzSY8vY9504WfArW7pGSboBToZMApgAS5v5NoVxAmaS7kPBWwJPwA2wGDCBuSQfm7axtzi0Z/7DU+AAG0mNf57WZOIkacf8o+87wn7GCVdtDrBpc1JTb5wkxWDTqEp1Ar5g1aaHeuMkaY6rbE8WlB9gp20xnxfAkaQLYFPLsgshX9E4ZWCztyVtJF0C5W7Xm1sSMZm3qROnHGxqsV1ayXWOcTW4xO28fdWJUy42RsEKKPfB1wYoJV1IquCUqAOc8DYpH8VEe2MzX7byieEEXBBQBKM3kk5wG1QBLCVd92Xe7EuLLfyS2WwdSbptchSpPU6SFmYHN6kOrYCyM/haImVTdUrUWtJ6VGy6cKpXp90AsfaLYBNKoo7TCyodm0AiWTi9GDaBBCo4jYxQHjahJCQdkbfZdWk4bNpkSSRtdq+CTZtIxOlVsQklYSuxil2FgO7IwCbnXPsWeO8P3GZ36B3whhe4simKYo/QUu5wn4JJq22+TiSNn4R9VXYewH1FJDL4RUFT4BP/kzhllhO0lrQYfCXkjoGVk1QuNm32KDhh132yk1SqEhMZDqcdNmQcwDOUh5OPzUCfxsl2Fk5kYmNBrPu89Fk44c6xudhsJP02/I4lXZF/suvGiYYDeP3jKzKJK+Cj+SskfZT0q+dkxONkM5a0STXoH/CB2mcFrmnydxSc5G7MDpoLqdgAPwl0UiTNJP1gSJxwS1ypNn5gsQjZJ/GCwEecjbWw3/aZpEOcbNA9Nj3r90ruajL4jnlJlHKdoOTgTc844VqZ26bAI7Fxt8SJIv+iYCvpeyHX6VjhuoEHQYYQUsZJiuf70ov6mF22XIPjTtLZQYspAaFobEJJ4FbiNgUhSQ/AGX7HEmvy4XVrhsYmkEgUTobNA22VboeT/07UEcrBJpBAJ04VbAKOpsAprpXZNAPZ2ISSCOHUiE3A2QzXytz9q8Hg2ATGruDUiU2batWp2pMaUT5OUdgEHO1wOqetJzWSPJzOu7D5D4CgMYHM5nJ2AAAAAElFTkSuQmCC" />
                                <S.Span>CLOSE</S.Span>
                            </S.ImageContainer>
                        </Grid>
                        {items
                            ? items.map(item => (
                                  <Grid item xs={item.double ? 12 : 6} sm={item.double ? 6 : 3}>
                                      <a href={item.url}>
                                          <S.Card cover={item.cover} bgimage={item.image}>
                                              <S.Title>{item.title}</S.Title>
                                          </S.Card>
                                      </a>
                                  </Grid>
                              ))
                            : ''}
                    </Grid>
                </S.Container>
            </div>
        )
    }
}
