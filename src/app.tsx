import * as React from 'react'

type appType = () => React.ReactNode

// tslint:disable-next-line:variable-name
export const App: appType = (): React.ReactNode => <div />
