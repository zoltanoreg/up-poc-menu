import styled from 'styled-components'
import React from 'react'

export const Input = styled.input`
    ::placeholder {
    color: white;
    font-size: 18px;
    opacity: .8;
    }
    outline: none;
    color: white;
    font-size: 18px;
    border: none;
    height: 46px;
    border-radius: 8px;
    background-color: #404040;
    text-align: center;
    width: 100%;
    background-size: 30px 30px;
    background-position: 10px;
    background-repeat: no-repeat;
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAGoUlEQVRogcWZf2heVxnHP9+X8FJGiSGUUnoucY5ShpYx9scUrTqlnXXMH1SQgtvq/AH+oxJFRGWMUrpShg4nmKWVahHL3EQrStVSZExwc8wxYumqdLHIPaWUEEIJIYSQr3/ck3Dfm/d98943a/KFkMu955znfN/znOd8n+eIPpHn+YCkDNgJ7AE+DOwCtttuSloApoHLwB+AiyGEmX7tdYLqdogxNoF7gX3Aftt7gG0rA0rYXnlOmLb9G0lPA1dDCOud9wpqEYgxDgFHgQPACNCs0X0ReN32mKQXQgjzdWx3Qk8E8jy/Q9JDwDFgN4DtlV+4l+cyEdsvAKNZlt287QRijA3b35L0TSDr0nQRmAHmga3AINDo0v4kcCSEcL3GfFehK4EYYwP4CvB0mtDKr5r8fEnSNeA88CfbU4lIM23wTwCPAFvaDD8H/Ag4EUKYfccJxBibtr8EHJM03Mb4ZeA48OcQwlyncfI83yXpu8BBYLjiVnPAaAjh5O0g8IDtcUm7y+9tT0s6DYyFECZLE21kWbbUYaxB4JDtE5KGKp+vAR/q15XaEogxDgB/AT5e+bRo++uSzgK3bA9LOgjsp/D73wNn27lEGvMbwJHUtuyOT0l6IoTQ9gfohlWbLM/zhu2vtZn8jO3RLMueS/0ek/QWcAr4PPAQ8BPgs+0MhRAWgedsP0ux0Vcg6ZDtD9SdfFsCkkYkfaH8zvaC7eclnc3zfAgYBX4MbK90b9p+XydjIYQ5SWeAiWRrefxM0r48z+ucK6sJxBgBPgLcXSF1FRgHpiUdonCFqi9j+xbwSjeDtq8CL6bn5fGbwPslbevSdW0CwDCFP1cn9/Msy96kOH1PVL/bXgBeBg5LOt/NYNroP7M9W5YdwL22d6yXwHYKYVbGPPB8en4EGCwZBbgh6Vng0SzLziVf74oQwoyk16BFO21vE65rExgG7ipP0PYEkCcdtH/ZaMIcMGb7SAjhfzVtv7H8kEgMsHpPrYkWAmkJB8v6RdJLST3uoXChMiaAk1mW9XOS/jfZXLYDbfbVWqiuQLtN9Hb6f6ftobJR268DN+oaTX0XoVV+AwN1x2khkKIBFRe6mec5wA5JW5eNpv9/61fbL9sqjwes+yCbrwxIyqwGgHclP13GAvBSXYMltKxmQkdN1QlVAtNt2gxKWgT+Kel6yejfgb71vO33QIsLLdmuPV7V56ZTTG+WVmF3CIE8zy9I+rLtz0iaA369ntRQ0gdLz8sicd0EZiTdoDXafC7G+FSSzBfS37oQY9xLOu1L8voSfaxoNYzOAJOVd3so5MU7ghjjFuDxSvhckjTRjwtVo9BNiti+VHrXAEbzPK+tUzpgr+19lVx5GngluWYttBBIbvI7oHqqPijpaIyx9klZRozxbts/kNRyINp+w/aFfvZUu6T7VeB86bDCdpNCBz2Z5HRtpH5PAnvL720vSRrPsqxdBFwTbTOyPM9HJP0DqKrDBeCc7WPApU4pZBUxxoxi8l9kdeC4CHyq3zpRt5z4AEUOUF1ugFngnKRTwDXbU1mWzZX6AmyxvUPSAYoEqCW3pthnE8DhEMJEP5Nfi8AW4BBwNGVMq4pWFAnOm8AV229LmqEoqwwB77Z9H7BX0iqNY3tS0reBP/YiwWsTSCTusP1gSgMH1xhrgUKKLFHUgZp0LmzdAh6lKMks1JtyK3oqLcYY76Hw4Qds10o6KuFyHngNeIYiH5i1PdPrXmo7fq8N8zzfluqjh23fD2ytURtdsn0NOCNpCvgYRfJ0HfgXcNH2pSzLaq9Grep0nucNSZnteyR9Evg0xf1AtxroBPCi7QtJpvzQ9sF0QELhepO2XwbGUu59ewhUkWqnOykizAhF5GlImqdIdC6VU80Y453AGTpLk0XbP5V0yvZ/elmRdRGoixjjTorQ/HCXZosUqzYOnF4rQnVb+tuBWdZOQQeA+ygKx0+kumpHbDSBW8BbVEqLHTAMfB/4ZYzxvZ0abSiBEAK2LwNTPXYZAB62/UySI6uw0StA0v11KhkNSfuA4zHGVZJ+wwmEEK5LqpvVNSgq4N+LMW6tftgM/Ire9kEZTeCg7Zb9sFkErtBHbm17RFLLPcKmEEg3MUep5N9rIZ3eLWnbZq0AFJeE4+lOoQ5aDrZNIxBCmLN9Gvhtr31sLwH/Lr/bzBUgy7IpSV8FfkFxyHVEyp3/SnGRsoJNJQDF5Z/t71DsiStdmr4KHLfdUjHZUDHXDSmF3UURKj8K3CWpmUqOF4ExYLIq7v4P5LX/6VjxOpEAAAAASUVORK5CYII=');
}
`

export const Hamburger = styled(props => <div {...props}>{props.children}</div>)`
    width: 40px;
    height: 40px;
    float: right;
    position: relative;
    cursor: pointer;
    display: ${props => (props.opened ? 'none' : 'block')};
`

export const ImageWithText = styled.img`
    width: 30px;
    height: 30px;
    margin: auto;
    display: block;
`

export const Span = styled.span`
    position: absolute;
    bottom: 0;
    left: 0;
    color: red;
    font-size: 8px;
    right: 0;
    text-align: center;
    letter-spacing: 3px;
`
export const Overlay = styled(props => <div {...props}>{props.children}</div>)`
    position: fixed;
    z-index: 10;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: #404040;
    opacity: 0.5;
    display: ${props => (props.opened ? 'block' : 'none')};
`

export const Container = styled(props => <div {...props}>{props.children}</div>)`
    background: black;
    width: 540px;
    display: ${props => (props.opened ? 'block' : 'none')};
    position: absolute;
    right: 0;
    top: 0;
    z-index: 11;
    padding: 20px;
`

export const Image = styled.img`
    width: 40px;
    height: 40px;
    cursor: pointer;
`

export const ImageContainer = styled.div`
    width: 40px;
    height: 40px;
    float: right;
    position: relative;
    cursor: pointer;
`

export const Card = styled(props => <div {...props}>{props.children}</div>)`
    :hover {
        transform: scale(1.1);
    }
    border-radius: 8px;
    position: relative;
    height 120px;
    background-color: #404040;
    background-image: url(${props => props.bgimage});
    background-size: ${props => (props.cover ? 'cover' : '60px 60px')};
    background-repeat: no-repeat;
    background-position: ${props => (props.cover ? 'center' : 'center 20px')};
    boreder-radius: 5px;
    transition: transform .3s ease;
`

export const Title = styled.p`
    text-align: center;
    position: absolute;
    top: 66%;
    transform: translateY(-50%);
    padding: 5px;
    left: 0;
    right: 0;
    color: white;
`
