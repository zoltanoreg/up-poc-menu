export interface IMenu {
    items: IItem[]
}

interface IItem {
    type: any
    title: string
    url: string
    image: string
    double?: boolean
    cover?: true
}

export interface IMenuState {
    opened: boolean
}
