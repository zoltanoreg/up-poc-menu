import * as React from 'react'

export interface IHelloProps {
    /** Hello TSDoc generator! */
    name: string
}

export class Hello extends React.Component<IHelloProps> {
    public render(): React.ReactNode {
        return <h1>{this.props.name}</h1>
    }
}
