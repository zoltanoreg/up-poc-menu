import { storiesOf } from '@storybook/react'
import * as React from 'react'
import { Menu } from '../src/menu'
import { JssProvider } from 'react-jss'
import { generateClassName, jss } from './config/jss-config'

const menu = require('./jsons/menu.json')

const items = {}
storiesOf('Menu', module).add('Test', () => (
    <JssProvider jss={jss} generateClassName={generateClassName}>
        <div>
            <Menu items={menu} />
        </div>
    </JssProvider>
))
