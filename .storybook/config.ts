import { addParameters, configure } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'
import { addDecorator } from '@storybook/react'
import centered from '@storybook/addon-centered/react'
import { withInfo } from '@storybook/addon-info'

// Configure up addons
addDecorator(withKnobs)
addDecorator(withA11y)
addDecorator(centered)
addParameters({
    backgrounds: [
        {
            name: 'Grey',
            value: '#f9f9f9',
        },
        {
            name: 'White',
            value: '#ffffff',
            default: true,
        },
    ],
})
addDecorator(withInfo)

// Automatically import all files ending in *.stories.tsx
const req = require.context('../stories', true, /.stories.tsx$/)

const loadStories = () => {
    req.keys().forEach(req)
}

configure(loadStories, module)
